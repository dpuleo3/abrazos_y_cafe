import React, { Component } from 'react';
import '../assets/styles/userForm.css';

import axios from 'axios';

export default class UserForm extends Component {
  constructor() {
    super()
    this.state = {
  
      startDate: new Date(),
  
      email: '',
      password: '',
  
    }
  }

  componentDidMount() {}

    handleChange(e) {
      this.setState({ 
      [e.target.name]: e.target.value
     });
     console.log(`${e.target.name} now is: ${e.target.value}`)
    }
  

  sendForm = () => {
    let data = {
      email: this.state.email,
      password: this.state.password,
    }

    //Envio de la informacion al back
    axios.post('http://localhost:3001/signup', data)
    .then( response => { 
    
      console.log(response) 
    })
    .catch( err => {console.log(err, 'Algo salio mal') })
  }

  render() {

    return (
       
      <div className="login_form_wrapper">
        <h1>Entra a tu perfil</h1>
        <form > {/* aqui deberia ir un onSubmit */}
          <div className= "email">
            <label htmlFor="email_label">Correo Electrónico: </label>
            <input 
              className="email"
              type= "text" 
              required
              defaultValue= {this.state.email}
              onChange={this.handleChange.bind(this)} 
                />
          </div>
          <div className= "password">
            <label htmlFor= "password_label">Contraseña: </label>
            <input 
              className="password"
              type= "password"  
              required
              defaultValue= {this.state.password}
              onChange={this.handleChange.bind(this)}
            />
          </div>
          <button className="form_button" onClick={this.sendForm()}>
            <p>Enviar</p>
          </button>
        </form>
      </div> 
    );
  }

}

