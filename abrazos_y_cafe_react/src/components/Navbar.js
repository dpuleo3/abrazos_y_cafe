import React, { Component } from 'react';
import '../assets/styles/header.css';

export default class Navbar extends Component {
  render() {
    return (
       
      <nav className= "home_nav">
        <div className= "nav_wrapper">
          <div className= "nav_logo" onClick={event => window.location.href= './'}>
            <p>A&C</p>
          </div>
          <div className= "nav_login">
            <div className= "login_button" onClick={event => window.location.href= './login'}>
              <p>Login</p>
            </div>
            <div className= "nav_underline"></div>
          </div>
        </div>
      </nav>
      
    );
  }
}


