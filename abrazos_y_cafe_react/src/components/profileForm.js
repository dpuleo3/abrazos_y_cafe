import React, { Component } from 'react';
import '../assets/styles/profileForm.css';
// import axios from 'axios';

export default class ProfileForm extends Component {
  
  state = {

    startDate: new Date(),

    // avatar: '',
    full_name: '',
    email:'',
    occupation: '',
    bio: '',
    coffe_price: '',
    currency_symbol: '',
    multiplier: '',

  }

  handleChange(e) {
    this.setState({ 
    [e.target.name]: e.target.value
   });
  }

  showData = () => {
    console.log(`${this.state.profile_full_name_input} : Nombre y Apellido`)
    console.log(`${this.state.profile_email_input} : Email`)
    console.log(`${this.state.profile_bio_input} : Biografia`)
    console.log(`${this.state.profile_coffe_price_input} : Monto de Cafe`)
    console.log(`${this.state.profile_currency_symbol_input} : Simbolo de la Moneda`)
    console.log(`${this.state.profile_multiplier_input} : Multiplicador`)
  }

  // sendForm = () => {
  //   let data = {
  //     full_name: this.state.full_name,
  //     occupation: this.state.occupation,
  //     bio: this.state.bio,
  //     coffe_price: this.state.coffe_price,
  //     currency_symbol: this.state.currency_symbol,
  //   }
  //   //Envio de la informacion al back
  //   // axios.post('http://localhost:3001/requests',  data)
  //   // .then( response => { 
  //   // 
  //   //   console.log(response) 
  //   // })
  //   // .catch( err => {console.log(err, 'NO ha funcionado') })
  // }

  render() {

    return (

      <div className= "profile_form_wrapper">
        <h1>Crea tu perfil</h1>
        <form> {/* aqui deberia ir un onSubmit */}
          <div className= "profile_full_name">
            <label htmlFor= "full_name_label">Nombre y Apellido </label>
            <input 
              type= "text"
              className= "profile_full_name_input"
              required
              defaultValue= {this.state.full_name}
              onChange={this.handleChange.bind(this)} />
          </div>
          <div className= "profile_email">
            <label htmlFor="profile_email_label">Correo Electrónico </label>
            <input 
              type= "text" 
              className= "profile_email_input" 
              required
              defaultValue= {this.state.email}
              onChange={this.handleChange.bind(this)} />
          </div>
          <div className= "profile_bio">
            <label htmlFor= "profile_bio_label">Biografia </label>
              <input 
                type= "text-area"
                className= "profile_bio_input"
                required
                defaultValue= {this.state.bio}
                onChange={this.handleChange.bind(this)} />
          </div>
          <div className= "profile_coffe_price">
            <label htmlFor= "profile_coffe_price_label">Monto de Cafe </label>
              <input 
                type= "text"
                className= "profile_coffe_price_input"
                required
                defaultValue= {this.state.coffee_price}
                onChange={this.handleChange.bind(this)} />
          </div>
          <div className= "profile_currency_symbol">
            <label htmlFor= "profile_currency_symbol_label">Simbolo de la Moneda </label>
              <input 
                placeholder= "$"
                list= "currency_symbols"
                className= "profile_currency_symbol_input"
                required
                defaultValue= {this.state.currency_symbol}
                onChange={this.handleChange.bind(this)} />
                <div className= "currency_datalist">
                  <datalist id= "currency_symbols">
                    <option value= "$"></option>
                    <option value= "€"></option>
                    <option value= "BsS"></option>
                  </datalist>
                </div>
          </div>
          <div className= "profile_multiplier">
            <label htmlFor= "profile_multiplier_label">Multiplicador </label>
              {/* <input 
                type= "step"
                className= "profile_multiplier_input"
                required
                defaultValue= {this.state.currency_symbol}
                onChange={this.handleChange.bind(this)} /> */}
              <div className= "profile_multiplier_numbers">
                <button className= "number_2">2</button>
                <button className= "number_4">4</button>
                <button className= "number_6">6</button>
                <button className= "number_8">8</button>
              </div>
          </div>
          <div className= "profile_avatar">
            <label htmlFor= "profile_avatar_label">Avatar</label>
            <input 
              type= ""
              className= "profile_avatar_input"
              required
              defaultValue= {this.state.avatar}
              onChange={this.handleChange.bind(this)} />
          </div>
          <button className="form_button" onClick={event => {this.showData();
               window.location.href= '/bank_account'}}>
            <p>Enviar</p>
          </button>
        </form>
      </div>

    )
  }

}

