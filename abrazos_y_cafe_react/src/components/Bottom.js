import React, { Component } from 'react';
import '../assets/styles/bottom.css';
import pick_1 from '../assets/images/pick_1.png';
import pick_2 from '../assets/images/pick_2.png';
import pick_3 from '../assets/images/pick_3.png';

export default class Bottom extends Component {
  render() {
    return (

      <footer>
        <div className= "foot_wrapper">
          <div className= "foot_title">
            <h3>Un perfil dedicado a recibir agradecimientos</h3>
            <div className= "foot_underline"></div>
          </div>
          <div className= "foot_text">
            <p>Puedes crear una pagina en minutos sin conocimientos
              tecnicos. Muestra tus datos y agradecimientos 
              recibidos de otras personas.
            </p>
          </div>
          <div className= "profiles_images">
            <img className= "first_image" src= {pick_1} alt="first"/>
            <img className= "second_image" src= {pick_2} alt="second"/>
            <img className= "third_image" src= {pick_3} alt="third"/>
          </div>
        </div>
      </footer>

    );
  }
}
