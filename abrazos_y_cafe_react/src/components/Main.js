import React, { Component } from 'react';
import '../assets/styles/main.css';
import coffees from '../assets/images/coffees.png';

export default class Main extends Component {
  render() {
    return (

      <main>
        <div className= "main_wrapper">
          <img className= "three_coffees" 
            src= {coffees}
            alt="coffess"/>
          <div className= "main_title">
            <h3>Un cafe es un abrazo atrapado en una taza</h3>
          </div>
          <div className= "main_subtitle">
            <h6>Recibe aprecio y gratitud mediante donaciones online</h6>
          </div>
          <div className= "main_button">
            <div type= "button" onClick={event =>  window.location.href='/profile'}>
              <p>Crear mi perfil</p>
            </div>
          </div>
        </div>
      </main> 
      
    );
  }
}
