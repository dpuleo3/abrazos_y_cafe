import React, { Component} from 'react';
import '../assets/styles/bankForm.css';
// import axios from 'axios';

export default class BankForm extends Component {
  
  state = {

    startDate: new Date(),

    email: '',
    ci: '',
    nombre_y_apellido: '',
    nombre_de_cuenta: '',
    tipo_de_cuenta: '',
    numero_de_cuenta: '',

  }

    handleChange(e) {
      this.setState({ 
      [e.target.name]: e.target.value
     });
    }

    showData = () => {
      console.log(`${this.state.profile_email_input} : Email`)
      console.log(`${this.state.profile_ci_input} : CI`)
      console.log(`${this.state.profile_bank_name_input} : Nombre del Banco`)
      console.log(`${this.state.profile_account_type_input} : Tipo de Cuenta`)
      console.log(`${this.state.profile_account_number_input} : Numero de Cuenta`)
    }
  

  // sendForm = () => {
  //   let data = {
  //     email: this.state.email,
  //     password: this.state.password,
  //   }

    //Envio de la informacion al back
    // axios.post('http://localhost:3001/requests',  data)
    // .then( response => { 
    // 
    //   console.log(response) 
    // })
    // .catch( err => {console.log(err, 'NO ha funcionado') })
  //}
  
  render() {
    return (
      
        <div className= "bank_form_wrapper"> 
          <h1>Afilia tu cuenta bancaria</h1>
            <form> {/* aqui deberia ir un onSubmit */}
              <div className= "profile_email">
                <label htmlFor="profile_email_label">Correo Electrónico </label>
                <input 
                  className="profile_email_input"
                  type= "email" 
                  required
                  defaultValue= {this.state.email}
                  onChange={this.handleChange.bind(this)} 
                    />
              </div>
              <div className= "profile_ci">
                <label htmlFor="profile_ci_label">Cedula </label>
                <input 
                  className="profile_ci_input"
                  type= "number" 
                  required
                  defaultValue= {this.state.ci}
                  onChange={this.handleChange.bind(this)} 
                    />
              </div>
              <div className= "profile_bank_name">
                <label htmlFor= "bank_name_label">Nombre del Banco </label>
                <input 
                  type= "text"
                  className= "profile_bank_name_input"
                  required
                  defaultValue= {this.state.bank_name}
                  onChange={this.handleChange.bind(this)} />
              </div>
              <div className= "profile_account_type">
                <label htmlFor= "account_type_label">Tipo de Cuenta </label>
                <input 
                  type= "text"
                  className= "profile_account_type_input"
                  required
                  defaultValue= {this.state.account_type}
                  onChange={this.handleChange.bind(this)} />
              </div>
              <div className= "profile_account_number">
                <label htmlFor= "account_number_label">Numero de Cuenta </label>
                <input 
                  type= "number"
                  className= "profile_account_number_input"
                  required
                  defaultValue= {this.state.account_type}
                  onChange={this.handleChange.bind(this)} />
              </div>
              <button className="form_button" onClick={this.showData()}>
                <p>Enviar</p>
              </button>
            </form>
        </div>

    );
  }
}

