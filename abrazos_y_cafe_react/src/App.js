     import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Profile from './pages/Profile';
import Bank from './pages/Bank';



function App() {
  return (
    <Switch>
      <Route path='/' exact component={Home}/>
      <Route path='/login' exact component={Login}/>
      <Route path='/profile' exact component={Profile}/>
      <Route path='/bank_account' exact component={Bank}/>

      {/* <Route path='/list' exact component={List}/>
      <Route path='/request/:id' exact component={Request}/>
      <Route path='/movie/:id' exact component={Show}/>  */}

    </Switch>

  );
}

export default App;
