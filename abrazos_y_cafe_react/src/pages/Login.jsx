import React from 'react';
import Navbar from '../components/Navbar';
import UserForm from '../components/userForm';

class Login extends React.Component {

 render() {

  return (
    <>
      <Navbar/>
      <div className="login_page">
        <UserForm/>
        
      </div>
    </>
  )
 }
 
}

 
export default Login;