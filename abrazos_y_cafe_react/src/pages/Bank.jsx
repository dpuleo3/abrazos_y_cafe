import React from 'react';
import Navbar from '../components/Navbar';
import BankForm from '../components/bankForm';

class Bank extends React.Component {

  render() {
    return (
      <>
        <Navbar/>
        <div className= "bank_page"> 
          <BankForm/>
        </div>
      </>
    );
  }
}

export default Bank;
