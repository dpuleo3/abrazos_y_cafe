import React from 'react';
import Navbar from '../components/Navbar';
import ProfileForm from '../components/profileForm';

// import '../assets/styles/login.css';

class Profile extends React.Component {

 render() {
  return (
    <>
      <Navbar/>
      <div className="profile_page">
        <ProfileForm/>
      </div>
    </>
  )
 }
}

 
export default Profile;