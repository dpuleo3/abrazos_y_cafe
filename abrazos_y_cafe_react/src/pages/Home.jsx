import React from 'react';
import Navbar from '../components/Navbar';
import Main from '../components/Main';
import Bottom from '../components/Bottom';


class Home extends React.Component {

 render() {
  return (
    <>
      <div className="landing_page">
        <Navbar/>
        <Main/>
        <Bottom/>
      </div>
    </>
  )
 }
 }

 
export default Home;

